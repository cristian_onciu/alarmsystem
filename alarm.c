/*
 * alarm.c
 *
 *  Created on: Apr 29, 2013
 *      Author: Cristian Onciu
 */
#include <string.h>
#include <stdio.h>
#include <syslog.h>

FILE *fp_switch,*fp_speaker,*fp_sensor,*fp_led,*fp_mail;
int switch_pressed;

int setup_speaker(void);
int setup_switch(void);
int setup_sensor(void);
int read_sensor();
int start_speaker();
int stop_speaker();
int switch_status=0;
int armed=0;
char movement='1';
char mail_message[]="FROM: cristionciu@gmail.com\nTO: cristionciu@gmail.com\nSUBJECT: Alarm\nAlarm Started";
int mail_sent=0;

int main(int argv, char** argc) {
setup_speaker();
setup_switch();
setup_sensor();
while(1)
{
	fp_switch = fopen("/sys/class/gpio/gpio38/value", "rb+");
	switch_pressed=fgetc(fp_switch);
	if(switch_pressed!=switch_status)
		{
		if(switch_pressed=='0' && !armed)
			{
			openlog ("Alarm", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL0);
			syslog (LOG_INFO, "Alarm is ARMED");
			closelog();
			//activate led on the board
			fp_led = fopen("/sys/class/leds/a/brightness", "wb+");
			fwrite("1", sizeof(char), 1, fp_led);
			fclose(fp_led);
			armed=1;
			}
		else if (switch_pressed=='0' && armed){
			openlog ("Alarm", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL0);
			syslog (LOG_INFO, "Alarm was DISARMED!");
			closelog ();
			//dezactivate led on the board
			fp_led = fopen("/sys/class/leds/a/brightness", "wb+");
			fwrite("0", sizeof(char), 1, fp_led);
			fclose(fp_led);
			armed=0;
			mail_sent=0;
			stop_speaker();
			}
		switch_status=switch_pressed;
		}
	fclose(fp_switch);
	if(armed==1 && read_sensor()=='0')
		{
		if(!mail_sent)
			{	
			fp_mail = fopen("/root/mail_alert", "wb+");
			fwrite(mail_message, sizeof(char), strlen(mail_message), fp_mail);
			fclose(fp_mail);
			system("cat /root/mail_alert | msmtp cristionciu@gmail.com");
			mail_sent=1;
			openlog ("Alarm", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL0);
			syslog (LOG_INFO, "Movement Detected !!! %d", getuid ());
			closelog ();
			}
		start_speaker();		
		}

}	
return 0;

}
int stop_speaker()
{
	fp_speaker = fopen("/sys/class/gpio/gpio35/value", "wb+");
	fwrite("0", sizeof(char), 1, fp_speaker);
	fclose(fp_speaker);
	return 0;
}

int start_speaker()
{
	fp_speaker = fopen("/sys/class/gpio/gpio35/value", "wb+");
	fwrite("1", sizeof(char), 1, fp_speaker);
	fclose(fp_speaker);
	return 0;
}


int read_sensor()
{
	fp_sensor = fopen("/sys/class/gpio/gpio37/value", "rb+");
	movement=fgetc(fp_sensor);
	fclose(fp_sensor);
	return movement;
}

int setup_sensor()
{
openlog ("Alarm", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL0);
if ((fp_sensor = fopen("/sys/class/gpio/export", "ab")) == NULL) {
		syslog (LOG_ERR, "Cannot open export file.");
		exit(1);
	}
	//Set pointer to begining of the file
	rewind(fp_sensor);
	//Write our value of "37" to the file
	fwrite("37", sizeof(char), 2, fp_sensor);
	fclose(fp_sensor);
	
	syslog (LOG_INFO, "...export file accessed, pin 37 now accessible (SENSOR)");
	
	//SET DIRECTION
	//Open the SENSOR sysfs file in binary for reading and writing, store file pointer in fp
	if ((fp_sensor = fopen("/sys/class/gpio/gpio37/direction", "rb+")) == NULL) {
		syslog (LOG_ERR, "Cannot open direction file for SENSOR.");
		exit(1);
	}
	//Set pointer to begining of the file
	rewind(fp_sensor);
	//Write our value of "in" to the file
	fwrite("in", sizeof(char), 3, fp_sensor);
	fclose(fp_sensor);
	syslog (LOG_INFO, "...direction for SENSOR port set to input");
	closelog ();
	return 0;
}


int setup_switch()
{
openlog ("Alarm", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL0);
if ((fp_switch = fopen("/sys/class/gpio/export", "ab")) == NULL) {
		syslog (LOG_ERR, "Cannot open export file.");
		exit(1);
	}
	//Set pointer to begining of the file
	rewind(fp_switch);
	//Write our value of "38" to the file
	fwrite("38", sizeof(char), 2, fp_switch);
	fclose(fp_switch);

	syslog (LOG_INFO, "...export file accessed, pin 38 now accessible (SWITCH)");
	
	//SET DIRECTION
	//Open the SWITCH sysfs file in binary for reading and writing, store file pointer in fp
	if ((fp_switch = fopen("/sys/class/gpio/gpio38/direction", "rb+")) == NULL) {
		syslog (LOG_ERR, "Cannot open direction file for SWITCH.");
		exit(1);
	}
	//Set pointer to begining of the file
	rewind(fp_switch);
	//Write our value of "in" to the file
	fwrite("in", sizeof(char), 3, fp_switch);
	fclose(fp_switch);
	syslog (LOG_INFO, "...direction for SWITCH port set to input");
	closelog ();
	return 0;
}

int setup_speaker()
{
openlog ("Alarm", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL0);
if ((fp_speaker = fopen("/sys/class/gpio/export", "ab")) == NULL) {
		syslog (LOG_ERR, "Cannot open export file.");
		exit(1);
	}
	//Set pointer to begining of the file
	rewind(fp_speaker);
	//Write our value of "35" to the file
	fwrite("35", sizeof(char), 2, fp_speaker);
	fclose(fp_speaker);

	syslog (LOG_INFO, "...export file accessed, pin 35 now accessible (SPEAKER)");
	
	//SET DIRECTION
	//Open the SPEAKER sysfs file in binary for reading and writing, store file pointer in fp
	if ((fp_speaker = fopen("/sys/class/gpio/gpio35/direction", "rb+")) == NULL) {
		syslog (LOG_ERR, "Cannot open direction file for SPEAKER.");
		exit(1);
	}
	//Set pointer to begining of the file
	rewind(fp_speaker);
	//Write our value of "out" to the file
	//strcpy(set_value, "out");
	fwrite("out", sizeof(char), 3, fp_speaker);
	fclose(fp_speaker);
	//printf("...direction for SPEAKER port set to output\n");
	syslog (LOG_INFO, "...direction for SPEAKER port set to output");
	closelog ();
	return 0;
}
